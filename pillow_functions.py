from PIL import Image, ImageDraw, ImageFilter, ImageFont
from PIL import ImageEnhance

def make_image_darker(source_image_object):
    width, height = source_image_object.size
    gradient = Image.new('L', (width, 1), color=0xFF)
    for x in range(width):
            gradient.putpixel((x, 0), int(255 * (1 - 1. * float(x) / width)))
    alpha = gradient.resize(source_image_object.size)
    black_im = Image.new('RGBA', (width, height), color=0x000000)
    black_im.putalpha(alpha)
    gradient_im = Image.alpha_composite(source_image_object, black_im)
    return gradient_im

def title_to_list_for_image(title_string):
    title_in_list = title_string.split(' ')
    max_len_string = 20
    result_list = []
    current_string = ''
    for word in title_in_list:
        current_string += ' {}'.format(word)
        if len(current_string) > max_len_string:
            result_list.append(current_string)
            current_string = ''
    result_list.append(current_string)
    return result_list

def generate_share_image(source_image_name, title, rubric, output_image_name):
    source_image = Image.open(source_image_name).resize((1200, 630))
    source_image = make_image_darker(source_image)

    title_list = title_to_list_for_image(title)
    font_title_line_height = 60
    font_rubric = ImageFont.truetype("fonts/FuturaPT-Bold/FuturaPT-Bold.ttf", 20)
    font_title = ImageFont.truetype("fonts/FuturaPT-Bold/FuturaPT-Bold.ttf", font_title_line_height)
    filtered_image = source_image.filter(ImageFilter.BoxBlur(10),)
    draw = ImageDraw.Draw(filtered_image)  # Пишем заголовок и рубрику
    draw.text((65, 165), rubric.upper(), (255, 255, 255), font=font_rubric)
    line_counter = 0
    for title_elem in title_list:
        current_height = 200 + font_title_line_height * line_counter
        draw.text((50, current_height), title_elem, (255, 255, 255), font=font_title)
        line_counter +=1

    logo_image = Image.open('craft_logo.png') # Добавляем логотип
    filtered_image.paste(logo_image, (65, 75), logo_image)

    filtered_image.save('new_image2.png', 'PNG')


# Пример рабочей функции
# source_image_name = 'source_image.png'
# title = 'Люби бога и делай, что хочешьЛюби бога и делай, что хочешь'
# rubric = 'Подборка'
# output_image_name = 'new_image2.png'
# generate_share_image(source_image_name, title, rubric, output_image_name)
